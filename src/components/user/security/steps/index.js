const requireModule = require.context('.', false, /\.vue$/)
// extract vue files inside modules folder
const modules = {}

requireModule.keys().forEach((fileName) => {
  const moduleName = fileName.replace(/(\.\/|\.vue)/g, '')

  modules[`${moduleName}`] = requireModule(fileName).default
})

export default modules
