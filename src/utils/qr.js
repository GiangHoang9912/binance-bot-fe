import { Encoder, Decoder, ErrorCorrectionLevel } from '@nuintun/qrcode'
const qrcodeToDecode = new Decoder()

const encoder = (string) => {
  const qrcodeToEncode = new Encoder()
  qrcodeToEncode.setEncodingHint(true)
  qrcodeToEncode.setErrorCorrectionLevel(ErrorCorrectionLevel.H)
  qrcodeToEncode.write(string)
  qrcodeToEncode.make()
  return qrcodeToEncode.toDataURL()
}

async function decoder(url) {
  try {
    const result = await qrcodeToDecode.scan(url)
    return result
  } catch (error) {
    console.error(error)
    return null
  }
}

const b64toBlob = (b64Data, contentType = '', sliceSize = 512) => {
  const byteCharacters = atob(b64Data)
  const byteArrays = []
  for (let offset = 0; offset < byteCharacters.length; offset += sliceSize) {
    const slice = byteCharacters.slice(offset, offset + sliceSize)
    const byteNumbers = new Array(slice.length)
    for (let i = 0; i < slice.length; i++) {
      byteNumbers[i] = slice.charCodeAt(i)
    }
    const byteArray = new Uint8Array(byteNumbers)
    byteArrays.push(byteArray)
  }
  const blob = new Blob(byteArrays, { type: contentType })
  // const blobUrl = URL.createObjectURL(blob);
  return blob
}

export { encoder, decoder, b64toBlob }
