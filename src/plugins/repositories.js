import createRepository from '~/repositories/Repository'

export default ({ $axios, $handleError }, inject) => {
  inject('repositories', createRepository($axios, $handleError))
}
