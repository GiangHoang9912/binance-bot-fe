import Vue from 'vue'
import { parseISO, format } from 'date-fns'

Vue.filter('formatDate', (date) => {
  if (!date) return '--/--/----'
  else return format(parseISO(date), 'dd/MM/yyyy')
})
