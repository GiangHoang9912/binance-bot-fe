const getDefaultState = () => {
  return {
    data: [],
  }
}

const state = () => getDefaultState()

const mutations = {
  RESET(state) {
    Object.assign(state, getDefaultState())
  },
  SET(state, data) {
    state.data = data
  },
}

const actions = {
  reset({ commit }) {
    commit('RESET')
  },
  set({ commit }, data) {
    commit('SET', data)
  },
}

export default {
  namespaced: true,
  state,
  mutations,
  actions,
}
