const getDefaultState = () => {
  return {}
}

const state = () => getDefaultState()

const mutations = {
  RESET(state) {
    Object.assign(state, getDefaultState())
  },
}

const actions = {
  reset({ commit }) {
    commit('RESET')
  },
}

export default {
  namespaced: true,
  state,
  mutations,
  actions,
}
