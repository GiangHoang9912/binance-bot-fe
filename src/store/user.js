const getDefaultState = () => {
  return {
    email: '',
    password: '',
    emailVerificationCode: '',
  }
}

const state = () => getDefaultState()

const mutations = {
  RESET(state) {
    Object.assign(state, getDefaultState())
  },
  SET_EMAIL(state, data) {
    state.email = data
  },
  SET_PASSWORD(state, data) {
    state.password = data
  },
  SET_EMAIL_VERIFICATION_CODE(state, data) {
    state.emailVerificationCode = data
  },
}

const actions = {
  reset({ commit }) {
    commit('RESET')
  },
  setEmail({ commit }, data) {
    commit('SET_EMAIL', data)
  },
  setPassword({ commit }, data) {
    commit('SET_PASSWORD', data)
  },
  setEmailVerificationCode({ commit }, data) {
    commit('SET_EMAIL_VERIFICATION_CODE', data)
  },
}

export default {
  namespaced: true,
  state,
  mutations,
  actions,
}
