const resource = 'user/payment'
export default ($axios) => ({
  async all() {
    return await $axios.$get(`${resource}`)
  },
})
