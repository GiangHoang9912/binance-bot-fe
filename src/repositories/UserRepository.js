const resource = 'user/auth'
export default ($axios) => ({
  async sendMail(val) {
    console.log('sendMail', val)
    return await $axios.$post(`/mail/send-confirmation-code`, val)
  },
  async register(val) {
    return await $axios.$post(`${resource}/register`, val)
  },
  async forgetPassword(val) {
    return await $axios.$post(`${resource}/forget-password`, val)
  },
  async info(id) {
    return await $axios.$get(`user/${id}`)
  },
  async getAllCoins() {
    return await $axios.$get(`https://api.coingecko.com/api/v3/coins/markets?vs_currency=usd&order=market_cap_desc&per_page=100&page=1&sparkline=true&price_change_percentage=1h%2C24h%2C7d`)
  },
  async getCoinLogo(id) {
    return await $axios.$get(`user/${id}`)
  },
  async getCoinChart(id) {
    return await $axios.$get(`user/${id}`)
  },
})
