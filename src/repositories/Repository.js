import UserRepository from '~/repositories/UserRepository'
import GoogleAuthenRepository from '~/repositories/GoogleAuthenRepository'
import BotRepository from '~/repositories/BotRepository'
import WalletRepository from '~/repositories/WalletRepository'
import PaymentRepository from '~/repositories/PaymentRepository'
import PackageRepository from '~/repositories/PackageRepository'
import SignalRepository from '~/repositories/SignalRepository'
export default ($axios) => ({
  user: UserRepository($axios),
  googleAuthen: GoogleAuthenRepository($axios),
  bot: BotRepository($axios),
  wallet: WalletRepository($axios),
  payment: PaymentRepository($axios),
  package: PackageRepository($axios),
  signal: SignalRepository($axios),
})
