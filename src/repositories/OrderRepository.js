const resource = 'order'
export default ($axios) => ({
  async create() {
    return await $axios.$get(`${resource}`)
  },
})
