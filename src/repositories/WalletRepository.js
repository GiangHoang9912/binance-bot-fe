const resource = 'wallet'
export default ($axios) => ({
  async myWallet() {
    return await $axios.$get(`${resource}/my-wallets`)
  },
  async purchasePackage(val) {
    return await $axios.$post(`${resource}/purchase-package`, val)
  },
  async makeDeposit(val) {
    return await $axios.$post(`${resource}/make-deposit`, val)
  },
})
