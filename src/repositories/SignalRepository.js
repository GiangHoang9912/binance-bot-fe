const resource = 'signal'
export default ($axios) => ({
  async show(botId) {
    return await $axios.$get(`${resource}/${botId}`)
  },
})
