const resource = 'user/auth/2fa'
export default ($axios) => ({
  async generateOtp(userId) {
    return await $axios.$post(`${resource}/generate-otp`)
  },
  async backupKey(val) {
    return await $axios.$post(`${resource}/backup-key`, val)
  },
  async getVerificationCode(userId) {
    return await $axios.$post(`${resource}/get-verification-code/${userId}`)
  },
  async activate2fa(val) {
    return await $axios.$post(`${resource}/activate-2fa`, val)
  },
})
