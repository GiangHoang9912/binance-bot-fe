const resource = 'bots'
export default ($axios) => ({
  async all() {
    return await $axios.$get(`${resource}/`)
  },
  async show(id) {
    return await $axios.$get(`${resource}/${id}`)
  },
})
