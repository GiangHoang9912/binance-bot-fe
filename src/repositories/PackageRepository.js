const resource = 'packages/'
export default ($axios) => ({
  async all() {
    return await $axios.$get(`${resource}`)
  },
  async purchasedPackages() {
    return await $axios.$get(`${resource}purchased`)
  },
})
