export default {
  // Disable server-side rendering: https://go.nuxtjs.dev/ssr-mode
  ssr: false,

  // Global page headers: https://go.nuxtjs.dev/config-head
  head: {
    title: 'Binan Trade',
    htmlAttrs: {
      lang: 'en',
    },
    meta: [ 
      { charset: 'utf-8' },
      { name: 'viewport', content: 'width=device-width, initial-scale=1' },
      { hid: 'description', name: 'description', content: '' },
      { name: 'format-detection', content: 'telephone=no' },
    ],
    link: [{ rel: 'icon', type: 'image/x-icon', href: '/logo.png' }],
    script: [
      {
        src: 'https://s3.tradingview.com/tv.js',
      },
    ],
  },

  // Global CSS: https://go.nuxtjs.dev/config-css
  css: [
    '@/assets/main.scss',
    'element-ui/lib/theme-chalk/index.css',
    '@/assets/element-ui.scss',
  ],

  // Plugins to run before rendering page: https://go.nuxtjs.dev/config-plugins
  plugins: [
    '@/plugins/element-ui',
    '@/plugins/persistedState.client',
    '@/plugins/repositories.js',
    '@/plugins/vClickOutside.js',
    '@/plugins/filter.js',
  ],

  // Auto import components: https://go.nuxtjs.dev/config-components
  components: true,

  // Modules for dev and build (recommended): https://go.nuxtjs.dev/config-modules
  buildModules: [
    // https://go.nuxtjs.dev/eslint
    '@nuxtjs/eslint-module',
    // https://go.nuxtjs.dev/tailwindcss
    '@nuxtjs/tailwindcss',
  ],

  // Modules: https://go.nuxtjs.dev/config-modules
  modules: ['@nuxtjs/axios', '@nuxtjs/auth-next'],

  // Build Configuration: https://go.nuxtjs.dev/config-build
  build: {},

  srcDir: 'src',

  // Axios module configuration: https://go.nuxtjs.dev/config-axios
  axios: {
    // Workaround to avoid enforcing hard-coded localhost:3000: https://github.com/nuxt-community/axios-module/issues/308
    baseURL: `https://${process.env.API_URL}`,
  },

  auth: {
    localStorage: false,
    strategies: {
      local: {
        scheme: 'refresh',
        token: {
          property: 'data.accessToken',
          maxAge: 3600,
          global: true,
        },
        refreshToken: {
          property: 'data.refreshToken',
          data: 'refreshToken',
          maxAge: 60 * 60 * 24 * 30,
        },
        endpoints: {
          login: {
            url: 'user/auth/login',
            method: 'post',
            propertyName: 'data.accessToken',
          },
          logout: {
            url: 'user/auth/logout',
            method: 'post',
          },
          user: false,
        },
        // tokenRequired: true,
        tokenType: 'Bearer',
        autoLogout: true,
      },
    },
    redirect: {
      login: '/auth/login',
      logout: '/auth/login',
      // callback: '/login',
      home: '/',
    },
  },

  router: {
    middleware: ['auth'],
  },

  tailwindcss: {
    config: {
      theme: {
        screens: {
          // sm: '576px',
          // // => @media (min-width: 576px) { ... }

          // Tablet
          md: '769px',
          // => @media (min-width: 769px) { ... }

          // Desktop
          lg: '1025px',
          // // => @media (min-width: 1025px) { ... }
        },
      },
      // plugins: [require('@tailwindcss/line-clamp')],
    },
  },
}
